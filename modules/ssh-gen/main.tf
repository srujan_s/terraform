resource "tls_private_key" "private_key_pair" {
    algorithm = "RSA"
}

resource "local_file" "private_key" {
    filename = "${var.namespace}-key.pem"
    sensitive_content = tls_private_key.private_key_pair.private_key_pem
    file_permission = "0400"
}
resource "aws_key_pair" "key" {
    key_name = "${var.namespace}-key"
    public_key = tls_private_key.private_key_pair.public_key_openssh
}
