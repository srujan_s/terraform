output "key_name" {
    value = aws_key_pair.key.key_name
}

output "ssh_key_pair" {
    value = tls_private_key.private_key_pair.private_key_pem
}