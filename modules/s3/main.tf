resource "aws_s3_bucket" "bucket1" {
    bucket = var.bucket_name
    acl = "private"
    tags = {
      Environment = "Dev"
      Name = "buckbuck"
    }
}

resource "aws_s3_bucket_object" "objects" {
    bucket = var.bucket_name
    key = var.key_name
    source = "./${var.key_name}.pem"
}