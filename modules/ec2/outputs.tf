output "public_ip" {
    value = aws_instance.serverPub.public_ip
}
output "private_ip" {
    value = aws_instance.serverPub.private_ip
}