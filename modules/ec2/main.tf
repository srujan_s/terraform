resource "aws_instance" "serverPub" {
    ami = var.ami
    instance_type = var.type
    associate_public_ip_address = true
    subnet_id = var.vpc.public_subnets[0]
    vpc_security_group_ids = [var.sg_pub_id]
    key_name = var.key_name
    tags = {
      Name = "${var.namespace}-public"
    }
    provisioner "file" {
        source = "./${var.key_name}.pem"
        destination = "/home/ec2-user/${var.key_name}.pem"
        connection {
            type = "ssh"
            user = "ec2-user"
            private_key = file("./${var.key_name}.pem")
            host = self.public_ip
        }
    }
    provisioner "remote-exec" {
        inline = ["chmod 400 ${var.key_name}.pem"]
        connection {
            type = "ssh"
            user = "ec2-user"
            private_key = file("./${var.key_name}.pem")
            host = self.public_ip
        }
    }
    user_data = <<-EOF
                #!/bin/bash
                sudo yum install httpd -y
                sudo systemctl start httpd
                EOF
}

resource "aws_instance" "serverPri" {
    ami = var.ami
    instance_type = var.type
    associate_public_ip_address = false
    subnet_id = var.vpc.private_subnets[0]
    key_name = var.key_name
    vpc_security_group_ids = [var.sg_pri_id]
    tags = {
        Name = "${var.namespace}-private"
    }
}