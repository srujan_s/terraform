data "aws_availability_zones" "available" {}

module "vpc" {
    source = "terraform-aws-modules/vpc/aws"
    name = "${var.namespace}-vpc"
    cidr = "${var.vpc_cidr}"
    azs = data.aws_availability_zones.available.names
    private_subnets = ["10.0.0.0/24","10.0.1.0/24"]
    public_subnets = ["10.0.2.0/24","10.0.3.0/24"]
    enable_nat_gateway = true
    create_database_subnet_group = true
    single_nat_gateway = true
}

resource "aws_security_group" "sg_pub" {
    name = "${var.namespace}-firstsg"
    description = "this group is for public servers ${var.namespace}"
    vpc_id = module.vpc.vpc_id
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "sg_pri" {
    name = "${var.namespace}-secondsg"
    description = "secound security group for private servers ${var.namespace}"
    vpc_id = module.vpc.vpc_id
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["${var.vpc_cidr}"]
    }
}