output "sg_pri_id" {
    value = aws_security_group.sg_pri.id
}

output "sg_pub_id" {
    value = aws_security_group.sg_pub.id
}
output "vpc" {
    value = module.vpc
}