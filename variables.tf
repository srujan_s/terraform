variable "region" {
  default = "ap-south-1"
}
variable "namespace" {
  default = "teja"
}