output "ssh" {
    value = "ssh -i ${module.ssh-gen.key_name}.pem ec2-user@${module.ec2.public_ip}"
}
output "ssh2" {
    value = "ssh -i ${module.ssh-gen.key_name}.pem ec2-user@${module.ec2.private_ip}"
}